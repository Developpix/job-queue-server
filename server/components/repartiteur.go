package components

import (
	"fmt"
)

 /**
  * Cette fonction créer nbTravailleurs travailleurs.
  */
func creerTravailleurs(availableWorkers chan chan int, nbTravailleurs int) {
	for i := 0; i < nbTravailleurs; i++ {
		go CreerTravailleur(availableWorkers)
	}
}

 /**
  * Cette fonction créer le répartiteur.
  */
func CreerRepartiteur(fromCollector chan int, nbTravailleurs int) {
	//Création du canal de canal availableWorkers qui contient la liste des canaux des travailleurs disponibles.
	availableWorkers := make(chan chan int)

	// Cette goroutine permet le lancement en parallèle de la création des travailleurs.
	go creerTravailleurs(availableWorkers, nbTravailleurs)

	/**
	 * le for() permet d'asocier la tâche au premier travailleur disponible.
	 * En cas de travailleur non-disponible, le répartiteur attend qu'un travailleur soit disponible.
	 */
	for {
		select {
		case nb := <-fromCollector:
			fmt.Println("From Collector", nb)
			select {
			case workerChan := <-availableWorkers:
				workerChan <- nb
			}
		}
	}
}
