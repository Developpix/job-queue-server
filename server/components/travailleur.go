package components

/**
 * Cette fonction créé un travailleur.
 */

func CreerTravailleur(availableWorkers chan chan int) {
	/**
	 * On créer le canal workerChan sur lequel il écoute le travailleur, qu'on met dans 
	 * la liste des canaux des travailleurs disponibles.
	 */
	workerChan := make(chan int)
	availableWorkers <- workerChan

	/**
	 * Le for() lit depuis le workerChan et dès qu'il y'a une entrée par le client,
	 * il éxécute le nombre de boucle à faire (nb).
	 */
	for {
		select {
		case nb := <-workerChan:
			for i := 0; i < nb; i++ {
			}
		}
		
		availableWorkers <- workerChan
	}
}
