package main

import (
	"bufio"
	"fmt"
	"job-queue-server/server/components"
	"net"
	"strconv"
	"strings"
	"sync"
)

func main() {
	//Le serveur écoute le port :8020 en attente d'un client.
	listener, err := net.Listen("tcp", ":8020")
	
	//Liste de pseudo : Eviter d'acoir 2 pseudos identiques.
	var pseudos []string
	var pVerrou sync.Mutex

	//Création du canal fromCollector, qui permet de stocker le nombre envoyer par les clients.
	var fromCollector chan int
	fromCollector = make(chan int)

	/**
	 * goroutine qui permet de créer un répartiteur, ainsi qu'en paramètre un 
	 * canal fromCollector, et initaliser le nombre de travailleurs disponibles.
	 */
	go components.CreerRepartiteur(fromCollector, 5)

	if err != nil {
		fmt.Println("Erreur", err)
	} else {
		fmt.Println("Ecoute sur le port 8020...")

		for {
			//Tant qu'il n'y a pas de client, le serveur affiche dans la console Attente d'un client ...
			fmt.Println("Attente d'un client ...")
			//Accepte la connexion d'un client.
			conn, err := listener.Accept()
			
			//RemoteAddr(), affiche l'adresse du client.
			fmt.Println("Connexion de", conn.RemoteAddr())

			if err != nil {
				fmt.Println("Erreur", err)
			} else {
				go func() {
					//Envoie "tki?" au client qui se connecte pour qu'il se nomme.
					conn.Write([]byte("tki?\n"))

					m := " "
					//lit le pseudo envoyer par le client.
					reader := bufio.NewReader(conn)

					accepte := false
					var pseudo string

					//Tant que le pseudo est pas accepter, il lit le pseudo reçu.
					for !accepte {
						pseudo, _ = reader.ReadString('\n')
						reader.Reset(conn)

						// Check pseudos
						pVerrou.Lock()
						accepte = true
						//Remplace les \n et les \r des chaines de caractères vide.
						pseudo = strings.Replace(strings.Replace(pseudo, "\n", "", -1), "\r", "", -1)
						//Regarde les doublons des pseudos.
						for _, p := range pseudos {
							if pseudo == p {
								accepte = false
								break
							}
						}
						pVerrou.Unlock()
						//Pseudo pas accepter, il affiche tkn!tki?.
						if !accepte {
							conn.Write([]byte("tkn!tki?\n"))
						}
					}
					//Ajoute le pseudo dans la liste des pseudos.
					pVerrou.Lock()
					pseudos = append(pseudos, pseudo)
					pVerrou.Unlock()

					//Le serveur envoie ok quand le pseudo est bon.
					conn.Write([]byte("ok!\n"))
					
					//Si il y'a un problème de lecture de message la connexion est coupée.
					for m != "quit\n" && m != "\n" {
						m, err = reader.ReadString('\n')
						if err != nil {
							break
						}

						/**
						 * Mets le message dans le canal fromCollector, si c'est pas quit, 
						 * une ligne vide et que c'est un nombre.
						 */
						if m != "quit\n" && m != "\n" && err == nil {
							nb, err := strconv.Atoi(strings.Replace(strings.Replace(m, "\n", "", -1), "\r", "", -1))
							if err != nil {
								fmt.Print(pseudo, " problème nombre ")
							} else {
								fmt.Print(pseudo, " a envoyé ", m)
								fromCollector <- nb
							}
						}

						//Renvoie "ok" au client.
						conn.Write([]byte("ok!\n"))
						reader.Reset(conn)
					}
					
					//On retire le pseudo de la liste des pseudos.
					pVerrou.Lock()
					for i, p := range pseudos {
						if pseudo == p {
							pseudos = append(pseudos[:i], pseudos[i+1:]...)
							break
						}
					}
					pVerrou.Unlock()

					//Fermeture de connexion.
					conn.Close()
				}()
			}
		}
	}
}
