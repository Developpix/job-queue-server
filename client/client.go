package main

import "net"
import "fmt"
import "os"
import "bufio"

func main() {

		 /**Se connecter au serveur par l'adresse ip '127.0.0.1' avec le port 8020, mettre une autre adresse ip si vous
		  * voulez dialoguer avec une personne extérieur 
		  */
	connexion, err := net.Dial("tcp", "127.0.0.1:8020")
	if err != nil {
		//En cas de problème sur le dialogue serveur / client, il vous renvoie une erreur
		fmt.Println("Problème à l'écoute du serveur")
		os.Exit(-1)
	}
	// RemoteAddr() affiche l'adresse ip du dialogue
	fmt.Println("Connexion au serveur :", connexion.RemoteAddr())
	for {
		readMsg(connexion)
		writeMsg(connexion)
	}
}

 /**
  * Cette fonction permet de lire un message venant du serveur
  */
func readMsg(connexion net.Conn) string {
	reader := bufio.NewReader(connexion)
	message, _ := reader.ReadString('\n')
	fmt.Println(message)
	return message
}

 /**
  * Cette fonction vous permet d'écrire un message dans le terminal pour l'envoyer vers le serveur
  */
func writeMsg(connexion net.Conn) {
	writer := bufio.NewWriter(connexion)
	input := bufio.NewReader(os.Stdin)
	pseudo, _ := input.ReadString('\n')
	_, _ = writer.WriteString(pseudo)
	writer.Flush()
}
