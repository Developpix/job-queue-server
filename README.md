# Job Queue Server


## Compilation

Placez le dossier `job-queue-server` dans le dossier `src` de votre dossier contenant vos fichiers de GO (GOPATH).

Lancez les commandes suivantes pour compiler le serveur et le client :
```bash
go install job-queue-server/client
go install job-queue-server/server
```

## Exécution

Pour lancer le client ou le serveur, placez-vous dans le dossier `bin` de votre dossier contenant vos fichiers de GO (GOPATH) et lancer l'une des commandes suivantes :
```bash
./server	# Pour lancer le serveur (à lancer avant le client)
./client	# Pour lancer le client
```

### /!\Remarque/!\
Avant de lancer le client aller dans le fichier `job-queue-server/client/client.go` et modifier la ligne suivante
```go
connexion, err := net.Dial("tcp", "127.0.0.1:8020")	// Changer 127.0.0.1 par l'addresse IP du serveur
```

PS : Pour obtenir l'ip du serveur, ouvrez un terminal sur le serveur et tapez :
```shell
ifconfig	# Sous GNU/Linux ou Mac OS

ipconfig	# Sous Windows
```

Utilisez l'IPv4 pour la connexion.

### Changement du port

Pour changer le port utilisé par le serveur aller dans le fichier `job-queue-server/server/server.go` et modifier la ligne suivante :
```go
listener, err := net.Listen("tcp", ":8020")	// Changer 8020 par le port du serveur (utilisé un port supérieur à 8000 car les ports inférieur sont réservés au système)
```

Pensez également à remodifier le fichier `job-queue-server/client/client.go` et à la ligne suivante :
```go
connexion, err := net.Dial("tcp", "127.0.0.1:8020")	// Garder l'IP que vous avez saisie mais changez 8020 par le nouveau port choisi
```

## Utilisation

Au début le serveur envoi au client `tki?` lorsque le client se connecte. Sur le client lorsque vous recevez ce message vous devez saisir votre pseudo et faire entrer. A partir de maintenant le serveur vous renverra `ok!` à chaque fois qu'il aura reçu votre message. Si vous recevez un `tkn!tki?`, c'est que votre pseudo est déjà utilisé, veuillez saisir un nouveau pseudo et faire entrer à nouveau.

Une fois reçu le `ok!` lié à l'envoi de votre pseudo vous pouvez saisir un nombre (correspondant au nombre de boucle que le travailleur devra réaliser) et appuyer sur entrer pour l'envoyer au serveur.

Sur le serveur, un message `[pseudo] a envoyé [message]` s'affiche à la réception du message du client par le serveur. Lorsque le répartiteur lit le nombre envoyé un message `From collector [nb]` s'affiche et lorsqu'un travailleur est associé à l'exécution un message `Début de l'éxecution de [nombre] boucles` s'affiche. :smiley: